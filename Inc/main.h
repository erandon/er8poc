/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LFBL12_Pin GPIO_PIN_2
#define LFBL12_GPIO_Port GPIOE
#define SHARP2_Pin GPIO_PIN_3
#define SHARP2_GPIO_Port GPIOE
#define LFBL15_Pin GPIO_PIN_4
#define LFBL15_GPIO_Port GPIOE
#define LFBL1_Pin GPIO_PIN_5
#define LFBL1_GPIO_Port GPIOE
#define START_Pin GPIO_PIN_6
#define START_GPIO_Port GPIOE
#define LFBL13_Pin GPIO_PIN_0
#define LFBL13_GPIO_Port GPIOC
#define LFSHARP_Pin GPIO_PIN_1
#define LFSHARP_GPIO_Port GPIOC
#define LFBL2_Pin GPIO_PIN_2
#define LFBL2_GPIO_Port GPIOC
#define LFBL6_Pin GPIO_PIN_3
#define LFBL6_GPIO_Port GPIOC
#define CurSense1_Pin GPIO_PIN_4
#define CurSense1_GPIO_Port GPIOA
#define CueSense2_Pin GPIO_PIN_5
#define CueSense2_GPIO_Port GPIOA
#define CurSense3_Pin GPIO_PIN_6
#define CurSense3_GPIO_Port GPIOA
#define CurSense4_Pin GPIO_PIN_7
#define CurSense4_GPIO_Port GPIOA
#define LFBL8_Pin GPIO_PIN_4
#define LFBL8_GPIO_Port GPIOC
#define LFBL9_Pin GPIO_PIN_5
#define LFBL9_GPIO_Port GPIOC
#define Battery_Pin GPIO_PIN_0
#define Battery_GPIO_Port GPIOB
#define LFBL7_Pin GPIO_PIN_1
#define LFBL7_GPIO_Port GPIOB
#define PWM1A_Pin GPIO_PIN_9
#define PWM1A_GPIO_Port GPIOE
#define PWM1B_Pin GPIO_PIN_11
#define PWM1B_GPIO_Port GPIOE
#define PWM1C_Pin GPIO_PIN_13
#define PWM1C_GPIO_Port GPIOE
#define PWM1D_Pin GPIO_PIN_14
#define PWM1D_GPIO_Port GPIOE
#define LED5_Pin GPIO_PIN_10
#define LED5_GPIO_Port GPIOB
#define LED6_Pin GPIO_PIN_12
#define LED6_GPIO_Port GPIOB
#define SW1_Pin GPIO_PIN_13
#define SW1_GPIO_Port GPIOB
#define SW2_Pin GPIO_PIN_14
#define SW2_GPIO_Port GPIOB
#define SW3_Pin GPIO_PIN_15
#define SW3_GPIO_Port GPIOB
#define LED4_Pin GPIO_PIN_8
#define LED4_GPIO_Port GPIOD
#define LED3_Pin GPIO_PIN_9
#define LED3_GPIO_Port GPIOD
#define LED2_Pin GPIO_PIN_10
#define LED2_GPIO_Port GPIOD
#define LED1_Pin GPIO_PIN_11
#define LED1_GPIO_Port GPIOD
#define ENC3A_Pin GPIO_PIN_6
#define ENC3A_GPIO_Port GPIOC
#define ENC3B_Pin GPIO_PIN_7
#define ENC3B_GPIO_Port GPIOC
#define LFBL3_Pin GPIO_PIN_11
#define LFBL3_GPIO_Port GPIOA
#define ENC2A_Pin GPIO_PIN_15
#define ENC2A_GPIO_Port GPIOA
#define LFBL10_Pin GPIO_PIN_10
#define LFBL10_GPIO_Port GPIOC
#define LFBL11_Pin GPIO_PIN_11
#define LFBL11_GPIO_Port GPIOC
#define MOT4B_Pin GPIO_PIN_0
#define MOT4B_GPIO_Port GPIOD
#define MOT4A_Pin GPIO_PIN_1
#define MOT4A_GPIO_Port GPIOD
#define MOT3B_Pin GPIO_PIN_2
#define MOT3B_GPIO_Port GPIOD
#define MOT3A_Pin GPIO_PIN_3
#define MOT3A_GPIO_Port GPIOD
#define MOT2B_Pin GPIO_PIN_4
#define MOT2B_GPIO_Port GPIOD
#define MOT2A_Pin GPIO_PIN_5
#define MOT2A_GPIO_Port GPIOD
#define MOT1B_Pin GPIO_PIN_6
#define MOT1B_GPIO_Port GPIOD
#define MOT1A_Pin GPIO_PIN_7
#define MOT1A_GPIO_Port GPIOD
#define ENC2B_Pin GPIO_PIN_3
#define ENC2B_GPIO_Port GPIOB
#define LFBL4_Pin GPIO_PIN_6
#define LFBL4_GPIO_Port GPIOB
#define LFBL5_Pin GPIO_PIN_7
#define LFBL5_GPIO_Port GPIOB
#define SHARP6_Pin GPIO_PIN_0
#define SHARP6_GPIO_Port GPIOE
#define LFBL14_Pin GPIO_PIN_1
#define LFBL14_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
