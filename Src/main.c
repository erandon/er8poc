
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

#define LED_2_OFF HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,0)
#define LED_2_ON HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,1)
#define LED_3_OFF HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,0)
#define LED_3_ON HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,1)
#define LED_4_OFF HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,0)
#define LED_4_ON HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,1)
#define LED_5_OFF HAL_GPIO_WritePin(LED5_GPIO_Port,LED5_Pin,0)
#define LED_5_ON HAL_GPIO_WritePin(LED5_GPIO_Port,LED5_Pin,1)
#define LED_6_OFF HAL_GPIO_WritePin(LED6_GPIO_Port,LED6_Pin,0)
#define LED_6_ON HAL_GPIO_WritePin(LED6_GPIO_Port,LED6_Pin,1)


#define S1 HAL_GPIO_ReadPin(SW1_GPIO_Port,SW1_Pin)
//#define S3 (PINF & _BV(2))

#define START HAL_GPIO_ReadPin(START_GPIO_Port,START_Pin)

#define CEGLA !HAL_GPIO_ReadPin(LFSHARP_GPIO_Port,LFSHARP_Pin)

volatile double err, lastErr;
volatile double intSum = 0;

volatile uint8_t sensors[15];


void GetSensorData()
{
	sensors[0] = !HAL_GPIO_ReadPin(LFBL1_GPIO_Port,LFBL1_Pin);
	sensors[1] = !HAL_GPIO_ReadPin(LFBL2_GPIO_Port,LFBL2_Pin);
	sensors[2] = !HAL_GPIO_ReadPin(LFBL3_GPIO_Port,LFBL3_Pin);
	sensors[3] = !HAL_GPIO_ReadPin(LFBL4_GPIO_Port,LFBL4_Pin);
	sensors[4] = !HAL_GPIO_ReadPin(LFBL5_GPIO_Port,LFBL5_Pin);
	sensors[5] = !HAL_GPIO_ReadPin(LFBL6_GPIO_Port,LFBL6_Pin);
	sensors[6] = !HAL_GPIO_ReadPin(LFBL7_GPIO_Port,LFBL7_Pin);
	sensors[7] = !HAL_GPIO_ReadPin(LFBL8_GPIO_Port,LFBL8_Pin);
	sensors[8] = !HAL_GPIO_ReadPin(LFBL9_GPIO_Port,LFBL9_Pin);
	sensors[9] = !HAL_GPIO_ReadPin(LFBL11_GPIO_Port,LFBL11_Pin);
	sensors[10] = !HAL_GPIO_ReadPin(LFBL10_GPIO_Port,LFBL10_Pin);
	sensors[11] = !HAL_GPIO_ReadPin(LFBL12_GPIO_Port,LFBL12_Pin);
	sensors[12] = !HAL_GPIO_ReadPin(LFBL13_GPIO_Port,LFBL13_Pin);
	sensors[13] = !HAL_GPIO_ReadPin(LFBL14_GPIO_Port,LFBL14_Pin);
	sensors[14] = !HAL_GPIO_ReadPin(LFBL15_GPIO_Port,LFBL15_Pin);
}

float wagi[15] = {-13,-9,-7,-5,-3,-2,-1,0,1,2,3,5,7,9,13};

volatile int przestrzelony = 0;
int pre = 0;
volatile int ilosc = 0;
volatile int stop =0;
void CalculateError()
{
	ilosc = 0;
    int waga = 10;                        // wsp�czynnik wagi czujnik�w ustawiany w zale�no�ci od przestrzelenia; poprzednio sta�y i r�wny 10
    err = 0;
    if(przestrzelony)                    // zmniejszenie wag czujnik�w w przypadku przestrzelenia zakr�tu
	{
        waga = 5;
		LED_3_ON;
	}
	else
	{
		LED_3_OFF;
	}

    for(int i=0; i<15; i++)
    {
        err += sensors[i]*wagi[i]*waga;
        ilosc += sensors[i];
    }

    if(ilosc != 15)
    {
        err /= ilosc;
		pre = err;
		przestrzelony = 0;
    }
    else
    {
        if(pre < 0)
        {
            err = -15;
            przestrzelony = 1;                // ustawienie flagi - przestrzelony, linia po lewej
        }
        else if(pre > 0)
        {
			err = 15;
            przestrzelony = 2;                // ustawienie flagi - przestrzelony, linia po prawej
        }
        else
            err = 0;
    }

    if(przestrzelony == 1 && err >= 0)        // zerowanie flagi przestrzelenia zakr�tu po powrocie na �rodek linii
        przestrzelony = 0;
    else if(przestrzelony == 2 && err <= 0)
        przestrzelony = 0;

}

#define IS_I 0
#define IS_D 1
#define IS_P 1
#define mnoznik 1

#define MAX 1000/*Max do PWM */

volatile int czas=0;

//TURBO
float Tp=5, Td=20, Ti=0;
int stala = 100;
int Turbina = 1200;
//drag
//double Tp=10, Td=300,Ti=0;
//int stala = 500;
//int Turbina = 1200;

//deathrace
//double Tp=20, Td=100,Ti=0;
//int stala = 65;
//int Turbina = 775;

//LF
//double Tp=5, Td=2, Ti=0;
//int stala=15;
//int Turbina = 1000;


//bezpieczne 2.5 30 stala 22

volatile double sterowanie;

double absolut (double liczba)
{
if (liczba <0 ) return -1*liczba ;
else return liczba;
}

double RegulatePID(double dt)
{
	double sterowanie = 0;
	if(IS_P)
		sterowanie += Tp * err;
	if(IS_I)
	{
		intSum += err * dt / 100000.0;
		sterowanie += intSum / Ti;
	}
	if(IS_D)
	{
		sterowanie += Td*(err - lastErr) / dt;
	}
	lastErr = err;
	return sterowanie;
}

#define intervalTime 2

#define CEGLY_OMIJANIE 0
#define DEATHRACE_ON 1


void Jazda(int lewy, int prawy)
{
    if(lewy >= 0)
    {
		if(lewy>MAX)
			lewy = MAX;
		  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,0);
		  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,1);
    }
    else
    {
		if(lewy<-MAX)
			lewy = -MAX;
		  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,1);
		  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,0);
    }

    if(prawy >= 0)
    {
        if(prawy>MAX)
            prawy = MAX;
		  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,1);
		  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,0);
    }
    else
    {
        if(prawy<-MAX)
            prawy = -MAX;
		  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,0);
		  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,1);
    }

    TIM1->CCR3=1000-absolut(lewy);
    TIM1->CCR2=1000-absolut(prawy);
}

void Followtheline()
{
GetSensorData();
CalculateError();
sterowanie = RegulatePID(intervalTime);
Jazda(stala+mnoznik*sterowanie,stala-1*mnoznik*(sterowanie)); // normalne
//if (ilosc == 0)
//	Jazda (500,500);
//czas++;
//if ((stala <999) && (czas>300))
//stala++;
HAL_Delay(1);
/* if (przestrzelony==1)
	  while(1)
	  {
	  Jazda(-200,200);
	  CalculateError();
	  if (err!=0) break;
	  }
if (przestrzelony==3)
		  while(1)
		  {
		  Jazda(200,-200);
		  CalculateError();
		  if (err!=0) break;
		  }
		  */
}



int ominalem = 0;
int gdzie = 0;

volatile int z = 0;

int starastala;

volatile uint16_t pomiary[5];

void deathrace()
{
	if(CEGLA)
	{
		TIM1->CCR3=1000;//OCR3A=500;
		Jazda(0,0);
		HAL_Delay(5000);
		/*if (gdzie)
		Jazda(-50,50);
		else
		Jazda(50,-50);
		HAL_Delay(100);
		Jazda(100,100);
		HAL_Delay(50);
		if (gdzie)
		gdzie = 0;
		else
		gdzie = 1;
	*/
	}


}

int ceglaiterator;


void cegly_omijanie()
{
	if(CEGLA && !ominalem && CEGLY_OMIJANIE)
	{
		TIM1->CCR3=1000;

		ominalem = 0;
		//starastala=stala;
		//stala=-40;

		/*
		for (ceglaiterator = 0; ceglaiterator < 40; ceglaiterator++)
		{
		double sterowaniecegla = RegulatePID(intervalTime);
		Jazda(stala+mnoznik*sterowaniecegla,stala-1*mnoznik*(sterowaniecegla));
		HAL_Delay(4);

		OCR3A=800;
		HAL_Delay(200);
		OCR3A=500;
		}
		*/


		//w prawo
		//Jazda(30,-30);
		//w lewo
		Jazda (-100,100);
		HAL_Delay(75);

		//prosto
		Jazda(30,30);
		HAL_Delay(350);

		//w prawo
		Jazda(55,-55);
		HAL_Delay(250);

		//prosto
		Jazda(30,30);
		HAL_Delay(350);

		//w lewo
		//Jazda(-30,30);
		//p prawo
		Jazda(30,-30);
		HAL_Delay(150);

		//prosto
		Jazda(30,30);
		HAL_Delay(205);

		//stala=starastala;
while(1)
{
	Jazda(50,50);
	GetSensorData();
	CalculateError();
	if (ilosc !=0)
		TIM1->CCR3=Turbina;//OCR3A=Turbina;
	break;
	}
	ominalem=1;

		Jazda (-100,100);
		HAL_Delay(70);

		//prosto
		Jazda(30,30);
		HAL_Delay(350);

		//w prawo
		Jazda(55,-55);
		HAL_Delay(250);

		//prosto
		Jazda(30,30);
		HAL_Delay(350);

		//w lewo
		//Jazda(-30,30);
		//p prawo
		Jazda(30,-30);
		HAL_Delay(150);

		//prosto
		Jazda(30,30);
		HAL_Delay(205);

		//stala=starastala;
		while(1)
		{
			Jazda(50,50);
			GetSensorData();
			CalculateError();
			if (ilosc !=0)
				TIM1->CCR3=Turbina;//OCR3A=Turbina;
			break;
		}
		ominalem=1;

	}
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C3_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM10_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_2);
  TIM1->CCR1=800; //unused
  TIM1->CCR2=800; //prawy silnik
  TIM1->CCR3=800; //lewy silnik
  TIM1->CCR4=800; //unused
  TIM4->CCR1=1000; //turbina prawy slot
  TIM5->CCR2=1000; // lewy slot pusty
  HAL_GPIO_WritePin(MOT1A_GPIO_Port,MOT1A_Pin,0);
  HAL_GPIO_WritePin(MOT1B_GPIO_Port,MOT1B_Pin,0);
  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,0);
  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,0);
  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,0);
  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,0);
  HAL_GPIO_WritePin(MOT4A_GPIO_Port,MOT4A_Pin,0);
  HAL_GPIO_WritePin(MOT4B_GPIO_Port,MOT4B_Pin,0);

  //ENC 3AB - lewy silnik
  //ENC 2AB - prawy silnik

  HAL_ADC_Start_DMA(&hadc1, pomiary, 5);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  HAL_Delay(2000);
  HAL_Delay(2);
  TIM4->CCR1=1400; //turbina prawy slot
  //TIM5->CCR2=1400;

  HAL_Delay(1000);
  while(1)
  {
	  GetSensorData();
	  CalculateError();
	  while(START && !stop)
	  {
		  Followtheline();
		  cegly_omijanie();
		  if (CEGLA)
		  {
			  stop=1;
		  }
	  }
	  Jazda(0,0);
	  HAL_Delay(1);

  }


  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 84;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
